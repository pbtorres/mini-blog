import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyCekBVBjXVvCPuJ_0JiobldFIYcH6gbpSg",
  authDomain: "miniblog-94612.firebaseapp.com",
  projectId: "miniblog-94612",
  storageBucket: "miniblog-94612.appspot.com",
  messagingSenderId: "11464601084",
  appId: "1:11464601084:web:6610ac1291bf57bcb8d679"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);

export { db, auth };
