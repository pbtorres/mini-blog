import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import { useAuthValue } from '../context/AuthContext'

import Home from '../pages/home/Home'
import About from '../pages/about/About'
import Login from '../pages/login/Login'
import Register from '../pages/register/Register'
import CreatePost from '../pages/createPost/CreatePost'
import Dashboard from '../pages/dashboard/Dashboard'
import Search from '../pages/search/Search'
import Post from '../pages/post/Post'
import EditPost from '../pages/editPost/EditPost'

function RoutesApp() {

  const { user } = useAuthValue();

  return (
    <>
      <Routes>
        <Route path="/" element={ <Home /> }/>
        <Route path="/about" element={ <About /> }/>
        <Route path='/search' element={ <Search/> }/>
        <Route path='/posts/:id' element={ <Post/> }/>
        <Route path="/login" element={ !user ? <Login /> : <Navigate to='/'/> }/>
        <Route path="/register" element={ !user ? <Register /> : <Navigate to='/'/> }/>
        <Route path='/posts/create' element={ user ? <CreatePost/> : <Navigate to='/login'/> }/>
        <Route path='/posts/edit/:id' element={ user ? <EditPost/> : <Navigate to='/login'/> }/>
        <Route path='/dashboard' element={ user ? <Dashboard/> : <Navigate to='/login'/> }/>
      </Routes>
    </>
  )
}

export default RoutesApp