import './App.css';
import { BrowserRouter } from 'react-router-dom';

import { onAuthStateChanged } from 'firebase/auth';

//HOOKS
import { useState, useEffect } from 'react';
import { useAuthentication } from './hooks/useAuthentication';

//CONTEXT
import { AuthProvider } from './context/AuthContext';

//PAGES
import RoutesApp from './routes/RoutesApp';
import Navbar from './components/Navbar';
import Footer from './components/Footer';


function App() {

  const[user, setUser] = useState(undefined);
  const { auth } = useAuthentication();

  const loadingUser = user === undefined;

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setUser(user);
    });
  },[auth]);

  if(loadingUser) {
    return <p style={ { textAlign: 'center'} }>Loading ...</p>
  }

  return (
    <div className="App">
      <AuthProvider value={ { user } }>
        <BrowserRouter>
          <Navbar />

          <div className="container">
            <RoutesApp />
          </div>

          <Footer />
        </BrowserRouter>
      </AuthProvider>
    </div>
  );
}

export default App;
