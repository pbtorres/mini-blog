import React from 'react'
import styles from './Search.module.css'
import { Link } from 'react-router-dom';

//HOOKS
import { useFetchDocuments } from '../../hooks/useFetchDocuments'
import { useQuery } from '../../hooks/useQuery'

//COMPONENTS
import PostDetail from '../../components/PostDetail';

function Search() {
    const query = useQuery();
    const search = query.get("q");

    const { documents: posts } = useFetchDocuments("posts", search);

    return (
        <>
            <div className={styles.search_container}>
                <h2>Results founds to: '{search}'</h2>
                <div className="post-list">
                    {posts && posts.length === 0 && (
                        <div className={styles.no_posts}>
                            <p>Posts not found...</p>
                            <Link to='/' className='btn btn-dark'>Return</Link>
                        </div>
                    )}
                    {posts && posts.length > 0 && posts.map((post) => (
                        <PostDetail key={post.id} post={post} />
                    ))}
                </div>
            </div>
        </>
    )
}

export default Search
