import React, { useEffect, useState } from 'react'
import styles from './Login.module.css'
import { useAuthentication } from '../../hooks/useAuthentication';

function Login() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const { login, error: authError, loading } = useAuthentication();

    const handleSubmit = async (e) => {
        e.preventDefault();

        setError(false);

        const user = {
            email,
            password
        }

        console.log(user);
        const res = await login(user);

        console.log(res);
    }

    useEffect(() => {
        console.log(authError);
        if(authError) {
            setError(authError);
        }
    }, [authError]);

    return (
        <div className={styles.login}>
            <h1>Login</h1>
            <p>Log in to use system</p>
            <form onSubmit={ handleSubmit }>
                <label>
                    <span>Email:</span>
                    <input
                        type="email" name='email'
                        required
                        placeholder='Type your email'
                        value={ email }
                        onChange={ (e) => setEmail(e.target.value) }/>
                </label>
                <label>
                    <span>Password:</span>
                    <input
                        type="password"
                        name='password'
                        required
                        placeholder='Type your password'
                        value={ password }
                        onChange={ (e) => setPassword(e.target.value) }
                     />
                </label>
                { !loading && <button className='btn'>Login</button>}
                { loading && <button className='btn' disabled>Wait...</button> }
                { error && <p className='error'>{ error }</p> }
            </form>
        </div>
    )
}

export default Login