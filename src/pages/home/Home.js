import styles from './Home.module.css'
import { useNavigate, Link } from 'react-router-dom'
import { useState } from 'react'

//HOOKS
import { useFetchDocuments } from '../../hooks/useFetchDocuments';

//COMPONENTS
import PostDetail from '../../components/PostDetail';

function Home() {
  const { documents: posts, loading, error } = useFetchDocuments("posts");
  const navigate = useNavigate();
  const [query, setQuery] = useState("");


  const handleSubmit = (e) => {
    e.preventDefault();

    if(query) {
      return navigate(`/search?q=${query}`)
    }
  }

  return (
    <div className={ styles.home }>
      <h2>See the most recent posts</h2>
      <form onSubmit={ handleSubmit } className={ styles.search_form }>
        <input type="text"
          placeholder='Or search for tags...'
          onChange={(e) => setQuery(e.target.value)}
        />
        <button className='btn btn-dark'>Search</button>
      </form>

      <div className='post_list'>

        {/* LOADING POSTS*/}
        {loading && <p style={{ textAlign: 'center' }}>Loading posts...</p>}

        {/* NO POSTS */}
        {!loading && posts.length === 0 && (
          <div className={styles.noposts}>
            <p>No posts found</p>
            <Link to='/posts/create' className='btn'>Create first post</Link>
          </div>
        )}

        {/* POST LIST */}
        {!loading && posts.length > 0 && (
          <div>
            {posts.map((post) => <PostDetail key={post.id} post={post} />)}
          </div>
        )}
      </div>
    </div>
  )
}

export default Home