import styles from './EditPost.module.css'

import React from 'react'
import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useAuthValue } from '../../context/AuthContext'
import { useFetchDocument } from '../../hooks/useFetchDocument'
import { useUpdateDocument } from "../../hooks/useUpdateDocument";

function EditPost() {
  const { id } = useParams();
  const { document: post } = useFetchDocument("posts", id)

  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [body, setBody] = useState("");
  const [tags, setTags] = useState([]);
  const [formError, setFormError] = useState("");

  // FILL THE FORM
  useEffect(() => {
    if (post) {
      setTitle(post.title || "");
      setImage(post.image || "");
      setBody(post.body || "");

      setTags(post.tagsArray ? post.tagsArray.join(", ") : []);

    }
  }, [post]);

  // user
  const { user } = useAuthValue();
  const navigate = useNavigate();
  // insert document
  const { updateDocument, response } = useUpdateDocument("posts");

  const handleSubmit = (e) => {
    e.preventDefault();

    // validate image URL
    try {
      new URL(image);
    } catch (error) {
      setFormError("The image needs to be a URL");
      return
    }

    // create tags array
    const tagsArray = tags.split(",").map((tag) => tag.trim().toLowerCase())

    const data = { title, image, body, tags: tagsArray }

    updateDocument(id, data);

    // redirect to dashboard
    navigate("/dashboard");
  };

  return (
    <div className={styles.edit_post}>
      <h2>Editint post: '{post.title}'</h2>
      <p>Change post data how needs</p>

      <form onSubmit={handleSubmit}>
        {/* Title*/}
        <label>
          <span>Title:</span>
          <input
            type="text"
            name='title'
            placeholder='Think of a good title'
            required
            onChange={(e) => { setTitle(e.target.value) }}
            value={title} />
        </label>
        {/* Image */}
        <label>
          <span>Image URL:</span>
          <input
            type="text"
            name='image'
            placeholder='Insert a image'
            required
            onChange={(e) => { setImage(e.target.value) }}
            value={image} />
        </label>
        {/* Image Preview */}
        <p className={styles.preview_title}> Preview from current image</p>
        <img className={styles.image_preview} src={post.image} alt={post.title} />

        {/* Body */}
        <label>
          <span>Content:</span>
          <textarea
            name="body"
            required
            placeholder='Insert content of post'
            onChange={(e) => { setBody(e.target.value) }}
            value={body}
          ></textarea>
        </label>
        {/* Tags */}
        <label>
          <span>Tags:</span>
          <input
            type="text"
            name='tags'
            placeholder='Insert tags separated by commas'
            required
            onChange={(e) => { setTags(e.target.value) }}
            value={tags} />
        </label>
        
        {!response.loading && <button className='btn'>Save</button>}
        {response.loading && <button className='btn' disabled>Wait...</button>}
        
        {(response.error || formError) && (
          <p className="error">{response.error || formError}</p>
        )}

      </form>
    </div>
  )
}

export default EditPost