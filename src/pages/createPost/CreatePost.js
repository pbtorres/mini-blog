import styles from './CreatePost.module.css'

import React from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAuthValue } from '../../context/AuthContext'
import { useInsertDocument } from '../../hooks/useInsertDocument'

function CreatePost() {
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [body, setBody] = useState("");
  const [tags, setTags] = useState([]);
  const [formError, setFormError] = useState("");

  // user
  const { user } = useAuthValue();

  const navigate = useNavigate();

  // insert document
  const { insertDocument, response } = useInsertDocument("posts");

  const handleSubmit = (e) => {
    e.preventDefault();

    // validate image URL
    try {
      new URL(image);
    } catch (error) {
      setFormError("The image needs to be a URL");
      return
    }

    // create tags array
    const tagsArray = tags.split(",").map((tag) => tag.trim().toLowerCase())

    // check values
    if(!title || !image || !body || !tags) {
      setFormError("Fill in all fields, please")
    }

    if(formError) return;

    insertDocument({
      title,
      image,
      body,
      tagsArray,
      uid: user.uid,
      createdBy: user.displayName
    })

    // redirect
    navigate("/");
  };

  return (
    <div className={styles.create_post}>
      <h2>Create Post</h2>
      <p>Write about whatever you want!</p>

      <form onSubmit={ handleSubmit }>
        {/* Title*/}
        <label>
          <span>Title:</span>
          <input
            type="text"
            name='title'
            placeholder='Think of a good title'
            required
            onChange={(e) => { setTitle(e.target.value) }}
            value={title} />
        </label>
        {/* Image */}
        <label>
          <span>Image URL:</span>
          <input
            type="text"
            name='image'
            placeholder='Insert a image'
            required
            onChange={(e) => { setImage(e.target.value) }}
            value={image} />
        </label>
        {/* Body */}
        <label>
          <span>Content:</span>
          <textarea
            name="body"
            required
            placeholder='Insert content of post'
            onChange={(e) => { setBody(e.target.value) }}
            value={body}
          ></textarea>
        </label>
        {/* Tags */}
        <label>
          <span>Tags:</span>
          <input
            type="text"
            name='tags'
            placeholder='Insert tags separated by commas'
            required
            onChange={(e) => { setTags(e.target.value) }}
            value={tags} />
        </label>
        {!response.loading && <button className='btn'>Create</button>}
        {response.loading && <button className='btn' disabled>Wait...</button>}
        {(response.error || formError) && (
          <p className="error">{response.error || formError}</p>
        )}

      </form>
    </div>
  )
}

export default CreatePost