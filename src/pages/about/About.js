import React from 'react'
import styles from './About.module.css';
import { Link } from 'react-router-dom';

function About() {
    return (
        <div className={ styles.about }>
            <h2>
                About Mini <span>Blog</span>
            </h2>
            <p>This project consists of a blog made with React on front-end and Firebase on back-end.</p>

            <Link to="/posts/create" className='btn'>
                New post
            </Link>
        </div>
    )
}

export default About