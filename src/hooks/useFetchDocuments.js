import { useState, useEffect } from "react";
import { db } from "../firebase/config";
import { collection, query, orderBy, onSnapshot, where } from "firebase/firestore";

export const useFetchDocuments = (docCollection, search = null, uid = null) => {
    const [documents, setDocuments] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(null);

    /*Cleanup. deal with memory leak (lidando com vazamento de memória)*/
    const [cancelled, setCancelled] = useState(false);

    useEffect(() => {
        async function loadData() {
            if (cancelled) return;

            setLoading(true);

            const collectionRef = collection(db, docCollection);

            try {
                let q;

                //busca
                if (search) {
                    q = query(
                        collectionRef,
                        where("tagsArray", "array-contains",search), orderBy("creatAt", "desc"))
                } else if (uid) {
                    q = query(
                        collectionRef,
                        where("uid", "==", uid),
                        orderBy("creatAt", "desc"))
                } else {
                    q = query(collectionRef, orderBy("creatAt", "desc"));
                }
                
                await onSnapshot(q, (querySnapshot) => {
                    if (!querySnapshot.empty) {
                        const documentsData = querySnapshot.docs.map((doc) => ({
                            id: doc.id,
                            ...doc.data(),
                        }));

                        setDocuments(documentsData);
                        setLoading(false)
                    } else {
                        setDocuments([]);
                        setLoading(false)
                    }
                });
            } catch (error) {
                console.log("Error fetching documents:", error);
                setError(error.message)
                setLoading(false)
            }
        }

        loadData();
    }, [docCollection, search, uid, cancelled]);

    useEffect(() => {
        return () => setCancelled(true)
    }, []);

    return { documents, loading, error };
};