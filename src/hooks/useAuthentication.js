import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    updateProfile,
    signOut
} from "firebase/auth";

import { useState, useEffect } from "react";


export const useAuthentication = () => {
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(null);

    /*Cleanup. deal with memory leak (lidando com vazamento de memória)*/
    const [cancelled, setCancelled] = useState(false);

    const auth = getAuth();

    function checkIfIsCancelled() {
        if (cancelled) {
            return;
        }
    }

    //Register
    const createUser = async (data) => {
        checkIfIsCancelled();

        setLoading(true);
        setError(null);

        try {
            const { user } = await createUserWithEmailAndPassword(
                auth,
                data.email,
                data.password
            );

            console.log(user);

            await updateProfile(user, {
                displayName: data.displayName
            });

            setLoading(false);

            return user;

        }
        catch (error) {
            console.log(error.message);
            console.log(typeof error.message);

            let systemErrorMessage;

            if (error.message.includes("email-already")) {
                systemErrorMessage = "Email has already been used";
            }
            else if (error.message.includes("invalid-email")) {
                systemErrorMessage = "Invalid email";
            }
            else if (error.message.includes("Password")) {
                systemErrorMessage = "The password must be at least 6 characters long";
            } else {
                systemErrorMessage = "Somenthng happened, please try later";
            }

            setLoading(false);
            setError(systemErrorMessage);
        }
    };

    // Logout - sigin out
    const logout = () => {
        checkIfIsCancelled();
        signOut(auth);
        console.log("++ logout success ++");
    };

    // Login - Sigin
    const login = async (data) => {
        checkIfIsCancelled();

        setLoading(true);
        setError(null);

        try {
            await signInWithEmailAndPassword(auth, data.email, data.password);
            setLoading(false);
            console.log("++ Login success ++");
        }
        catch (error) {
            let systemErrorMessage;

            if (error.message.includes("invalid-credential")) {
                console.log(error.message);
                systemErrorMessage = "Invalid credentials";
            }
            else {
                console.log(error.message);
                systemErrorMessage = "Somenthing happened, please try later";
            }

            setError(systemErrorMessage);
            setLoading(false);
        }

    }

    useEffect(() => {
        return () => setCancelled(true);
    }, []);

    return {
        auth,
        createUser,
        error,
        loading,
        logout,
        login
    }
}