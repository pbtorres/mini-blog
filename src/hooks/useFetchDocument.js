import { useState, useEffect } from "react";
import { db } from "../firebase/config";
import { doc, getDoc } from "firebase/firestore";

export const useFetchDocument = (docCollection, id) => {
    const [document, setDocument] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(null);

    /*Cleanup. deal with memory leak (lidando com vazamento de memória)*/
    const [cancelled, setCancelled] = useState(false);

    useEffect(() => {
        async function loadDocumet() {
            if (cancelled) return;

            setLoading(true);

            try {
                const docRef = doc(db, docCollection, id);
                const docSnap = await getDoc(docRef);

                setDocument(docSnap.data());

                setLoading(false);
            } catch (error) {
                console.log("Error fetching documents:", error);
                setError(error.message);

                setLoading(false);
            }
        }

        loadDocumet();
    }, [docCollection, id, cancelled]);

    useEffect(() => {
        return () => setCancelled(true)
    }, []);

    return { document, loading, error };
};