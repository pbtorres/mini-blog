# Use a imagem oficial do Node.js como base
FROM node:14-alpine

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copie o arquivo package.json e o arquivo package-lock.json (ou yarn.lock, se você estiver usando o Yarn)
COPY package*.json ./

# Instale as dependências do projeto
RUN npm install

# Copie o restante dos arquivos do projeto para o diretório de trabalho
COPY . .

# Exponha a porta em que o servidor do aplicativo React está sendo executado
EXPOSE 3000

# Inicialize o servidor do aplicativo React
CMD ["npm", "start"]
